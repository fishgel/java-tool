package mysql;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;



/**
 * <p>
 *     根据markdown文档格式，生成sql语句。
 * </p>
 *
 *
 */
public class GenerateSql {

    public GenerateSql(){
        // TODO Auto-generated constructor stub
    }
    
    /**
     * @param args
     * 
     * 
     * 
        | product_name|  varchar(50)|  产品名称| 
        | product_id|  varchar(20)|  产品id| 
        
     * @throws IOException 
     */
    public static void main(String args[]) throws IOException{
        String filename = "D://sql.txt";
        BufferedReader in = new BufferedReader(new FileReader(filename));

        //表名
        String tableName = "hj_business_account_check_stream";
        System.out.println("CREATE TABLE `" + tableName + "` (");
        
        String s;
        while ((s = in.readLine()) != null) {
            String[] words = s.split("\\|");
            
            StringBuilder sb = new StringBuilder();
            if(words.length == 0){
                System.out.println("ss");
                continue;
            }

            
            int length = words.length;
          
            int start = 0;//找到第一个不为空的字段下标
            for(int i= 0;i<length;i++){
                if(!words[i].trim().isEmpty()){
                    start = i;
                    break;
                }
            }
            
            String fileName = words[start].trim();//字段名
            String type = words[start+1].trim();    //字段类型
            String comment = words[start+2].trim();//备注
            sb.append("`"+fileName+"`");
            sb.append(" ");
            sb.append(type);
            
            if(!fileName.trim().equals("comments")){
                sb.append(" NOT NULL  ");
            }
            
            //id字段不需要添加备注
            if(!fileName.trim().equals("id") && comment != null && !comment.isEmpty()){
                sb.append(" COMMENT '" + comment + "' ");
            }
            
            //主键需要 AUTO_INCREMENT 例如： `id` bigint(14) NOT NULL  AUTO_INCREMENT
            if(fileName.trim().equals("id")){
                sb.append("AUTO_INCREMENT");
            } 
            sb.append(",");
            
            System.out.println(sb.toString());
        }
        System.out.println("PRIMARY KEY (`id`)");
        System.out.println(")ENGINE=INNODB  DEFAULT CHARSET=utf8");
        in.close();
    }

}
